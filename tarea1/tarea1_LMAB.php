<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Lázaro Martínez Abraham Josué</title>
    <meta name="Description" content="Ejercicio 1 PHP">
    <link rel="stylesheet" href="css/main.css">
</head>
    <body>
        <h1>Piramide</h1>

        <div class="contenedor">
            <?php
            for ($x=0; $x < 30; $x++){
                echo "<div>";
                for ($y=0; $y <= $x; $y++){
                    echo "*";
                }
                echo "</div>\n";
            }
            ?>
        </div>

        <h1>Triangulo</h1>

        <div class="contenedor">
            <?php
            for ($x=0; $x < 15; $x++){
                echo "<div>";
                for ($y=0; $y <= $x; $y++){
                    echo "*";
                }
                echo "</div>\n";
            }
            for ($x=15; $x >=0 ; $x--){
                echo "<div>";
                for ($y=0; $y <= $x; $y++){
                    echo "*";
                }
                echo "</div>\n";
            }
            ?>
        </div>
    </body>
</html>

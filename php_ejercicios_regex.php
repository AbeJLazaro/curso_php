<?php
//Realizar una expresión regular que detecte emails correctos.
// Lazaro: El realizar una expresión regular que detecte todos los tipos distintos de emails correctos
// es una tarea titánica. Hay foros incluso en internet que se dedican a mejorar las expresiones regulares
// ya existentes para emails.

$correo = 'abrahamlazaro@comunidad.unam.mx';

// Creada por mi 
$regla = '/([^\s\:\;\?\¿\"\'\~]+@(([^\s\:\;\?\¿\"\'\~]+)\.)+([^\s\:\;\?\¿\"\'\~]+))/';

$result = preg_match($regla, $correo);
echo "Correos encontrados en " . $correo . ":<br/>";
echo $result;
echo "<br/>";

//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.

$curp = "ABCD123456EFGHIJ78";

$regla = "/([A-Z]{4}\d{6}[A-Z]{6}\d{2})/";

$result = preg_match($regla, $curp);
echo "CURPs encontrados en " . $curp . ":<br/>";
echo $result;
echo "<br/>";

//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.


$regla = "/(\w{51,})/";

$palabra = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
$result = preg_match($regla, $palabra);
echo "Palabras con más de 50 caracteres " . $palabra . ":<br/>";
echo $result;
echo "<br/>";

$palabra = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
$result = preg_match($regla, $palabra);
echo "Palabras con más de 50 caracteres " . $palabra . ":<br/>";
echo $result;
echo "<br/>";

//Crea una funcion para escapar los simbolos especiales.

$regla = '/([^\w\d\s])/i';
$palabra = "121213 1223 ad fasdf\\";
$result = preg_match($regla, $palabra);
echo "Caracteres especiales en " . $palabra . ":<br/>";
echo $result;
echo "<br/>";

//Crear una expresion regular para detectar números decimales.
// Esta expresión es sencilla, no detecta notación científica
$regla = '/(\d*.\d+)/i';
$palabra = "13.4";
$result = preg_match($regla, $palabra);
echo "Números decimales en " . $palabra . ":<br/>";
echo $result;
echo "<br/>";

$palabra = ".4";
$result = preg_match($regla, $palabra);
echo "Números decimales en " . $palabra . ":<br/>";
echo $result;
echo "<br/>";

?>
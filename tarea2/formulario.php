<?php
    session_start();
    if(!empty($_SESSION)){
        if(empty($_SESSION["iniciado"])){
            header('Location: login.php');
        }
    }else{
        header('Location: login.php');
    }

    if(!empty($_GET)){
        if($_GET["accion"] == "registrar"){
            if(empty($_SESSION["alumnos"])){
                $_SESSION["alumnos"] = [ 1 => $_POST];

            }else{
                array_push($_SESSION["alumnos"],$_POST);
            }
            unset($_SESSION[$_GET["accion"]]);	
        }
    }

?>

<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <meta name="Description" content="Login tarea 2">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="css/info.css">
</head>
<body>
    <!--BARRA DE NAVEGACIÓN-->
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-primary">
      <a class="navbar-brand mr-auto mr-lg-0" href="info.php">Home</a>
      <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="" >Registrar Alumnos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="cerrar.php" >Cerrar Sesión</a>
          </li>
        </ul>
      </div>
    </nav>

    <!--FORMULARIO-->
    <main role="main" class="container my-5 py-5">
        <form action="formulario.php?accion=registrar" id="form-registro" method="POST">
            <!--CAMPO NUMERO DE CUENTA-->
            <div class="row mb-3">
                <label for="num_cta" class="col-sm-2 col-form-label">Número de Cuenta</label>
                <div class="col-sm-10">
                    <input name="num_cta" type="text" class="form-control" id="num_cta" placeholder="Numero de cuenta" required="required">
                </div>
            </div>
            <!--CAMPO NOMBRE-->
            <div class="row mb-3">
                <label for="nombre" class="col-sm-2 col-form-label">Nombre</label>
                <div class="col-sm-10">
                    <input name="nombre" type="text" class="form-control" id="nombre" placeholder="Nombre" required="required">
                </div>
            </div>
            <!--CAMPO PRIMER APELLIDO-->
            <div class="row mb-3">
                <label for="primer_apellido" class="col-sm-2 col-form-label">Primer Apellido</label>
                <div class="col-sm-10">
                    <input name="primer_apellido" type="text" class="form-control" id="primer_apellido" placeholder="Primer Apellido" required="required">
                </div>
            </div>
            <!--CAMPO SEGUNDO APELLIDO-->
            <div class="row mb-3">
                <label for="segundo_apellido" class="col-sm-2 col-form-label">Segundo Apellido</label>
                <div class="col-sm-10">
                    <input name="segundo_apellido" type="text" class="form-control" id="segundo_apellido" placeholder="Segundo Apellido" required="required">
                </div>
            </div>
            <!--CAMPO GENERO-->
            <fieldset class="row mb-3">
                <label class="col-sm-2 col-form-label pt-0 ">Genero</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="genero" id="genero1" value="Hombre"  required="required">
                        <label class="form-check-label" for="genero1">
                        Hombre
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="genero" id="genero2" value="Mujer">
                        <label class="form-check-label" for="genero2">
                        Mujer
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="genero" id="genero3" value="Otro">
                        <label class="form-check-label" for="genero3">
                        Otro
                        </label>
                    </div>
                </div>
            </fieldset>
            <!--CAMPO FECHA DE NACIMIENTO-->
            <div class="row mb-3">
                <label for="fec_nac" class="col-sm-2 col-form-label">Fecha de Nacimiento</label>
                <div class="col-sm-10">
                    <input name="fec_nac" type="date" class="form-control" id="fec_nac"  required="required">
                </div>
            </div>
            <!--CAMPO CONTRASEÑA-->
            <div class="row mb-3">
                <label for="password" class="col-sm-2 col-form-label">Contraseña</label>
                <div class="col-sm-10">
                    <input name="password" type="password" class="form-control" id="password" placeholder="Contraseña"  required="required">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Registrar</button>
        </form>
    </main>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="js/offcanvas.js"></script>
</body>
</html>

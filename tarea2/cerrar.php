<?php 
    session_start();
    unset($_SESSION);
    unset($_POST);
    unset($_GET);
    session_destroy();

    header('Location: login.php');
?>
<?php
    session_start();
    
    if(!empty($_SESSION)){
        if(empty($_SESSION["iniciado"])){
            header('Location: login.php');
        }
    }else{
        header('Location: login.php');
    }

    function imprimir_datos_alumnos() {
        if(!empty($_SESSION["alumnos"])){
            foreach($_SESSION["alumnos"] as $key => $val) {
                echo "<tr>\n";
                echo '<th scope="row">' . $key . ' </th>';
                echo '<td>' . $val['nombre'] . ' ' . $val['primer_apellido'] . '</td>';
                echo '<td>' . $val['fec_nac'] . '</td>';
                echo '</tr>';
            }
        }else{
            echo "";
        }
    }



?>

<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <meta name="Description" content="Login tarea 2">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="css/info.css">
</head>
<body>
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-primary">
      <a class="navbar-brand mr-auto mr-lg-0" href="info.php">Home</a>
      <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="formulario.php" >Registrar Alumnos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="cerrar.php" >Cerrar Sesión</a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main" class="container my-5 py-5">
        <h2>Usuario autenticado</h2>
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php
                    echo $_SESSION['nombre'] . " " . $_SESSION['apellido'];
                    ?>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Información</h5>
                    <p class="card-text">Número de cuenta: 
                    <?php
                    echo $_SESSION['num_cta'];
                    ?>
                    </p>
                    <p class="card-text">Fecha de nacimiento: 
                    <?php
                    echo $_SESSION['fec_nac'];
                    ?>
                    </p>
                </div>
            </div>
        </div>
        <h2 class="pt-4">Datos Guardados:</h2>
        <div class="row">
        <table class="table">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Fecha Nacimiento</th>
                </tr>
            </thead>
            <tbody>
                <?php
                imprimir_datos_alumnos();
                ?>
            </tbody>
        </table>

        </div>
    </main>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="js/offcanvas.js"></script>
</body>
</html>

<?php
    if(!empty($_POST["num_cta"]) && !empty($_POST["pass"])){
        if(($_POST["num_cta"] == "1") && ($_POST["pass"] == "adminpass123")){
            // Iniciamos la sesión
            session_start();
            // Creamos/asignamos variables de la sesión
            $_SESSION['nombre'] = 'Admin';
            $_SESSION['apellido'] = 'General';
            $_SESSION['num_cta'] = '1';
            $_SESSION['genero'] = 'otro';
            $_SESSION['fec_nac'] = '14/03/1999';
            $_SESSION['iniciado'] = True;
            
            header('Location: info.php');
        }
    }
?>

<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <meta name="Description" content="Login tarea 2">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="css/login.css">
</head>
<body>
    <div class="login-form">
        <form action="login.php" method="post" class="border border-secondary">
            <h2 class="text-center">Login :D</h2>       
            <div class="form-group">
                <label class="float-left form-label">Numero de cuenta</label>
                <input name="num_cta" type="text" class="form-control" placeholder="" required="required">
            </div>
            <div class="form-group">
                <label class="float-left form-label">Contraseña</label>
                <input name="pass" type="password" class="form-control" placeholder="" required="required">
            </div>            
            <div class="form-group">
                <button type="submit" class="btn btn-info">Entrar</button>
            </div>  
        </form>
    </div>
</body>
</html>